﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Intermec.DeviceManagement.SmartSystem;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Net;
using System.IO;
using Intermec.DataCollection.RFID;
using System.Data.SqlServerCe;
//using System.ServiceModel.Web;
using System.Runtime.Serialization;
using System.Configuration;
//using System.Runtime.Serialization.





namespace KPC_QMS_Final
{

    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        

        string HttpGet(string uri, string parameters)
        {
            var request = WebRequest.Create(uri);

            var response = request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return responseString;
        }

        string HttpPost(string uri, string parameters)
        {
            // parameters: name1=value1&name2=value2	
            WebRequest webRequest = WebRequest.Create(uri);
            //webRequest.RequestUri.Port = 8201;
            //string ProxyString = 
            //   System.Configuration.ConfigurationManager.AppSettings
            //   [GetConfigKey("proxy")];
            //webRequest.Proxy = new WebProxy (ProxyString, true);
            //Commenting out above required change to App.Config
            webRequest.ContentType = "application/x-www-form-urlencoded";
            webRequest.Method = "POST";

            byte[] bytes = Encoding.ASCII.GetBytes(parameters);
            Stream os = null;
            try
            { // send the Post
                webRequest.ContentLength = bytes.Length;   //Count bytes to send
                os = webRequest.GetRequestStream();
                try
                {

                    os.Write(bytes, 0, bytes.Length);
                }       //Send it

                catch (WebException ex1)
                {
                    MessageBox.Show(ex1.Message, "HttpPost: Request error1",
                  MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            catch (WebException ex)
            {
                MessageBox.Show(ex.Message, "HttpPost: Request error",
                   MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            finally
            {
                if (os != null)
                {
                    os.Close();
                }
            }

            try
            { // get the response
                WebResponse webResponse = webRequest.GetResponse();
                if (webResponse == null)
                { return null; }
                StreamReader sr = new StreamReader(webResponse.GetResponseStream());
                return sr.ReadToEnd().Trim();
            }
            catch (WebException ex)
            {
                MessageBox.Show(ex.Message, "HttpPost: Response error",
                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
            return null;
        }

        

        private void btnLogin_Click(object sender, EventArgs e)
        {

            String mainURL = "http://kpcqms.com:7090/handheld/login/u/";
            String dURL = mainURL + txtuname.Text + "/p/" + txtPassword.Text;
            string Ret = Cons.HttpPost(dURL, " ");

            int pos = Ret.IndexOf(":", 0);
            string sRet = Ret.Substring(pos + 1, 1);
            if (sRet == "1")
            {
                pos = Ret.IndexOf(":", pos+1);
                moduleQMS.gCheckpoint = int.Parse(Ret.Substring(pos + 2, 1));
                frmQueue myForm = new frmQueue();
                this.Hide();
                myForm.ShowDialog();
            }
            else
            {
                string strMess = "";
                pos = Ret.IndexOf(":", pos + 1);
                strMess = Ret.Substring(pos + 2);
                strMess = strMess.Substring(0, strMess.Length - 2);
                MessageBox.Show(strMess);
            }

        }
        public static void ExecuteDML(string sql)
        {
            SqlCeCommand myCommand = new SqlCeCommand(sql);
            SqlCeConnection conn = new SqlCeConnection(Cons.connStr);
            conn.Open();
            myCommand.Connection = conn;
            //connOpen();
            myCommand.ExecuteNonQuery();
            conn.Close();
        }
        public bool jsonConverterCheckPoints(string strJson)
        {
            string jsonStr = strJson;//string to manipulate
            string sId = "", sPosition = "";
            string sTitle = "";
            bool bExit = false;
            string sQuery = "";
            jsonStr = jsonStr.Replace("[", "");
            jsonStr = jsonStr.Replace("]", "");
            jsonStr = jsonStr.Replace("{", "");
            jsonStr = jsonStr.Replace("}", "");
            jsonStr = jsonStr.Replace("\"", "");
            jsonStr = jsonStr + ",";
            int i = 1;
            //delete statement
            string sSqlDel = "DELETE [checkpoint]";
            ExecuteDML(sSqlDel);
            while (!bExit)
            {
                int pos = 0;
                int posNext = 0;
                int valueLen = 0;
                string strval = "";//posNext-pos+1);
                int cutPost = 0;

                for (i = 1; i <= 3; i++)
                {
                    pos = jsonStr.IndexOf(":", 0);
                    if (pos == -1)
                    {
                        bExit = true;
                        break;
                    }
                    posNext = jsonStr.IndexOf(",", 0);
                    valueLen = posNext - pos - 1;
                    strval = jsonStr.Substring(pos + 1, valueLen);
                    if (i % 3 == 1)
                    {
                        sId = strval;
                    }
                    else if (i % 3 == 2)
                    {
                        sPosition = strval;
                    }
                    else//mod =0
                    {
                        sTitle = strval;
                    }

                    cutPost = posNext + 1;
                    jsonStr = jsonStr.Substring(cutPost);
                }
                if (!bExit)
                {
                    //INSERT INTO [checkpoint](id, position, title) VALUES        (13, 1, NULL])
                    //string 
                    string sSql = "INSERT INTO [checkpoint] (id, position, title) VALUES (" + sId + "," + sPosition + ",'" + sTitle + "')";
                    ExecuteDML(sSql);
                    sQuery = sQuery + "(" + sId + "," + sPosition + "," + sTitle + "),";
                }
                i = 1;
            }
            return true;
        }
        //Checklists
        public bool jsonConverterCheckLists(string strJson)
        {
            string jsonStr = strJson;//string to manipulate
            string sId = "", sPosition = "";
            string sBody = "", sCheckpointId="";
            bool bExit = false;
            string sQuery = "";
            jsonStr = jsonStr.Replace("[", "");
            jsonStr = jsonStr.Replace("]", "");
            jsonStr = jsonStr.Replace("{", "");
            jsonStr = jsonStr.Replace("}", "");
            jsonStr = jsonStr.Replace("\"", "");
            jsonStr = jsonStr.Replace("'", "");
            jsonStr = jsonStr + ",";
            int i = 1,y=1;
            //delete statement
            string sSqlDel = "DELETE [checklists]";
            ExecuteDML(sSqlDel);
            while (!bExit)
            {
                int pos = 0;
                int posNext = 0;
                int valueLen = 0;
                string strval = "";//posNext-pos+1);
                int cutPost = 0;

                for (i = 1; i <= 4; i++)
                {
                    pos = jsonStr.IndexOf(":", 0);
                    if (pos == -1)
                    {
                        bExit = true;
                        break;
                    }
                    posNext = jsonStr.IndexOf(",", 0);
                    valueLen = posNext - pos - 1;
                    strval = jsonStr.Substring(pos + 1, valueLen);
                    if (i % 4 == 1)
                    {
                        sId = strval;
                    }
                    else if (i % 4 == 2)
                    {
                        sPosition = strval;
                    }
                    else if (i % 4 == 3)
                    {
                        sBody = strval;
                    }
                    else//mod =0
                    {
                        sCheckpointId = strval;
                    }

                    cutPost = posNext + 1;
                    jsonStr = jsonStr.Substring(cutPost);
                }
                if (!bExit)// && y >30)
                {
                    //INSERT INTO [checkpoint](id, position, title) VALUES        (13, 1, NULL])
                    //string 
                    string sSql = "INSERT INTO [checklists] (id, position, body, checkpointId) VALUES (" + sId + "," + sPosition + ",'" + sBody +"',"+ sCheckpointId + ")";
                    ExecuteDML(sSql);
                    sQuery = sQuery + "(" + sId + "," + sPosition + "," + sBody + "," + sCheckpointId +"),";
                }
                i = 1;
                y = y + 1;
            }
            return true;
        }

        private void menuRefresh_Click(object sender, EventArgs e)
        {
            //String mainURL = "http://192.168.1.100/qms_l/public/handheld/";
            //String dURL = mainURL + "checklists";
            //string Ret = HttpGet(dURL, " ");
            //bool ret = jsonConverterCheckLists(Ret);


            String mainURL = "http://47.74.130.113:7000/qms/public/handheld/";
            String dURL = mainURL + "checkpoints";
            string Ret = HttpGet(dURL, " ");
            bool ret = jsonConverterCheckPoints(Ret);
            MessageBox.Show(Ret, "return",
                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);


            mainURL = "http://47.74.130.113:7000/qms/public/handheld/";
            dURL = mainURL + "checklists";
            Ret = HttpGet(dURL, " ");
            ret = jsonConverterCheckLists(Ret);
            MessageBox.Show(Ret, "return",
                  MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        private void Login_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'truckDataSet.kpc_truck' table. You can move, or remove it, as needed.
            //this.kpc_truckTableAdapter.Fill(this.truckDataSet.kpc_truck);
            // TODO: This line of code loads data into the 'checkPointsDataSet.mains' table. You can move, or remove it, as needed.
            //this.mainsTableAdapter.Fill(this.checkPointsDataSet.mains);
            // TODO: This line of code loads data into the 'checkPointDataSet.checkPoints' table. You can move, or remove it, as needed.
            //this.checkPointsTableAdapter.Fill(this.checkPointDataSet.checkPoints);
            // TODO: This line of code loads data into the 'qMSLiteDataSet.checkPoints' table. You can move, or remove it, as needed.
            //this.checkPointsTableAdapter.Fill(this.qMSLiteDataSet.checkPoints);
            // TODO: This line of code loads data into the 'checkpointDataSet.checkPoints' table. You can move, or remove it, as needed.
          

        }
        //public static string connStr = "data source= \\test7.sdf";

       
        //private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        //{
           
        //    //conn.Open();
        //    //SQLiteCommand comm = new SQLiteCommand("Select * From checkPoints", conn);
        //    //using (SQLiteDataReader read = comm.ExecuteReader())
        //    //{
        //    //    while (read.Read())
        //    //    {
        //    //        comboBox1..Add(new object[] { 
        //    //    read.GetValue(0), 
        //    //    read.GetValue(read.GetOrdinal("Id")),  
        //    //    read.GetValue(read.GetOrdinal("Tittle"))
        //    //    });
        //    //    }
        //    //}

        //}

        //private void button1_Click(object sender, EventArgs e)
        //{
        //    //SqlCeDataAdapter adapter = new SqlCeDataAdapter();

        //    //SQLiteConnection conn = new SQLiteConnection(connStr);
        //    //conn.Open();
        //    //string query = "select * from [Checkpoint]";
        //    //adapter.SelectCommand = new SQLiteCommand(query, conn);

        //    //DataSet dset = new DataSet();
        //    //adapter.Fill(dset);

        //    SqlCeDataAdapter adapter = new SqlCeDataAdapter();

        //    SqlCeConnection conn = new SqlCeConnection(Cons.connStr);
        //    conn.Open();
        //    string query = "select * from [Checkpoint]";
        //    adapter.SelectCommand = new SqlCeCommand(query, conn);

        //    DataSet dset = new DataSet();
        //    adapter.Fill(dset);

        //    cmbCheckpoint.DataSource = dset.Tables[0];
        //    cmbCheckpoint.DisplayMember = "title";
        //    cmbCheckpoint.ValueMember = "Id";
        //}

        //private void cmbCheckpoint_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //    MessageBox.Show(cmbCheckpoint.SelectedValue.ToString());
        //}

        //Adding a DataContract class
        //[DataContract]
        class Checkpoints
        {
            //[DataMember]
            public int id { get; set; }
            //[DataMember]
            public string position { get; set; }
            //[DataMember]
            public string title { get; set; }
        }


        private void menuItem1_Click(object sender, EventArgs e)
        {
            TagCreate myForm = new TagCreate();
            this.Hide();
            myForm.ShowDialog();
            this.Close();
        }

        private void btnQueue_Click(object sender, EventArgs e)
        {
            frmQueue myForm = new frmQueue();
            this.Hide();
            myForm.ShowDialog();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        

       
      


       

      


       
    }
}