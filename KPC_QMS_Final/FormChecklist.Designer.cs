﻿namespace KPC_QMS_Final
{
    partial class FormChecklist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.listView1 = new System.Windows.Forms.ListView();
            this.button1 = new System.Windows.Forms.Button();
            this.btnSaveCheckList = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.lbChekList = new System.Windows.Forms.Label();
            this.lbRegNo = new System.Windows.Forms.Label();
            this.cbStatus = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Location = new System.Drawing.Point(3, 26);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(222, 151);
            this.listView1.TabIndex = 0;
            this.listView1.View = System.Windows.Forms.View.Details;
            this.listView1.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            this.listView1.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.listView1_ItemCheck);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(5, 266);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(108, 18);
            this.button1.TabIndex = 1;
            this.button1.Text = "Retrieve Queue";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnSaveCheckList
            // 
            this.btnSaveCheckList.Location = new System.Drawing.Point(117, 266);
            this.btnSaveCheckList.Name = "btnSaveCheckList";
            this.btnSaveCheckList.Size = new System.Drawing.Size(108, 18);
            this.btnSaveCheckList.TabIndex = 2;
            this.btnSaveCheckList.Text = "Save Check List";
            this.btnSaveCheckList.Click += new System.EventHandler(this.btnSaveCheckList_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(5, 285);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(108, 17);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "Clear List";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(117, 285);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(108, 17);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // lbChekList
            // 
            this.lbChekList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(224)))), ((int)(((byte)(255)))));
            this.lbChekList.ForeColor = System.Drawing.Color.Red;
            this.lbChekList.Location = new System.Drawing.Point(5, 181);
            this.lbChekList.Name = "lbChekList";
            this.lbChekList.Size = new System.Drawing.Size(218, 57);
            // 
            // lbRegNo
            // 
            this.lbRegNo.Location = new System.Drawing.Point(3, 3);
            this.lbRegNo.Name = "lbRegNo";
            this.lbRegNo.Size = new System.Drawing.Size(100, 20);
            // 
            // cbStatus
            // 
            this.cbStatus.Items.Add("1.Approved");
            this.cbStatus.Items.Add("2.Suspended");
            this.cbStatus.Items.Add("3.Rejected");
            this.cbStatus.Location = new System.Drawing.Point(5, 241);
            this.cbStatus.Name = "cbStatus";
            this.cbStatus.Size = new System.Drawing.Size(218, 22);
            this.cbStatus.TabIndex = 5;
            // 
            // FormChecklist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(246, 304);
            this.Controls.Add(this.cbStatus);
            this.Controls.Add(this.lbRegNo);
            this.Controls.Add(this.lbChekList);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnSaveCheckList);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listView1);
            this.Menu = this.mainMenu1;
            this.Name = "FormChecklist";
            this.Text = "FormQueue";
            this.Load += new System.EventHandler(this.FormQueue_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnSaveCheckList;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Label lbChekList;
        private System.Windows.Forms.Label lbRegNo;
        private System.Windows.Forms.ComboBox cbStatus;
    }
}