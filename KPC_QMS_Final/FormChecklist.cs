﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlServerCe; 
using System.Linq; 
using System.Net;
using System.IO; 


namespace KPC_QMS_Final
{
    public partial class FormChecklist : Form
    {
        private static FormChecklist instance = null;
        public static string VehicleReg ="";
        public static string VehicleTag = "";
        public FormChecklist()
        {
            InitializeComponent();
        }
        public static FormChecklist GetInstance
        {
            get
            {
                if (instance == null)
                    instance = new FormChecklist();
                return instance;
            }
        }
        //public FormQueue()
        //{
        //    InitializeComponent();
        //}
        ///---------------------------------------------------------
       
       // public static string connStrLocal = "Data Source = \\KPLC.sdf; Password = K1P2L3C";
        

        private void processQueueReader(string sqlQuery,string queueType)
        {
            string strCheckList;
            int iChecklistID;
            try
            {
                SqlCeDataReader reader = moduleQMS.SelectSqlCeSrvRowsQMS(sqlQuery);
                //create header and give a name name
                initializeQueueListView(queueType);
                while (reader.Read() == true)
                {
                    iChecklistID = int.Parse(reader["id"] + "");
                    strCheckList = reader["body"] + "";
                    //process
                    //create rows
                    populateQueueListView(strCheckList,iChecklistID);
                }
                reader.Dispose();
                moduleQMS.connCloseQMS();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "! ADVICE !", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
        }
        ///---------------------------------------------------------
        
        private void initializeQueueListView(string queueType) 
        {
            // Create a new ListView control.
           // listView1.Bounds = new Rectangle(10, 10, 800, 200);

            // Set the view to show details.
            listView1.View = View.Details;
            // Display check boxes.
            listView1.CheckBoxes = true;
            // Select the item and subitems when selection is made.
            listView1.FullRowSelect = true;

            // Create columns for the items and subitems.
            // Width of -2 indicates auto-size.
            listView1.Columns.Add(queueType, 1200, HorizontalAlignment.Left);
        }
        //create indivindual row function
        private void populateQueueListView(string strDesc,int strPos)
        {
            // Create  item and  sets of subitems for each item.
            ListViewItem item1 = new ListViewItem("(" + strPos.ToString() + ") " + strDesc);
            // Place a check mark next to the item.
            item1.Checked = false;
            item1.ForeColor = Color.Maroon;
            item1.Tag = strPos;

            ///add the row
            listView1.Items.Add( item1 );
            item1 = null;
        }

  
        /// --------------------------------------

        
        private void FormQueue_Load(object sender, EventArgs e)
        {
            string strCheckpointDesc = "";
            lbRegNo.Text = VehicleReg;
            //string strQuery = "select queueNo,vehicleNo + '-' + vehicleTag + '-' + driverName + '-' + transporter + '-' + loading + '-' + product AS driverName from [queue]";
            string strQuery = "SELECT id, body FROM checklists WHERE (checkpointId = " + moduleQMS.gCheckpoint + ")";
            ////create header and give a name name
            //initializeQueueListView("Check List Details");
            ////create rows
            //populateQueueListView("Shipper L.O No. available at PipeCor data", 33);
            listView1.Clear();
            lbChekList.Text = "";

            string checkcommand = "SELECT title FROM [checkpoint] WHERE (id =" + moduleQMS.gCheckpoint + ")";
            DataTable dt =new DataTable() ;
            moduleQMS.reurnSqlCeSrvRows(dt, checkcommand);

            if (dt.Rows.Count > 0)
            {
               strCheckpointDesc=dt.Rows[0]["title"].ToString(); 
            }
            processQueueReader(strQuery, strCheckpointDesc);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int icheckpointId = 1;
            //string strQuery = "select queueNo,vehicleNo + '-' + vehicleTag + '-' + driverName + '-' + transporter + '-' + loading + '-' + product AS driverName from [queue]";
            string strQuery = "SELECT id, body FROM checklists WHERE (checkpointId = " + icheckpointId +")";
            ////create header and give a name name
            //initializeQueueListView("Check List Details");
            ////create rows
            //populateQueueListView("Shipper L.O No. available at PipeCor data", 33);
            listView1.Clear();
            lbChekList.Text = "";
            processQueueReader(strQuery,"Check List Details");
        }

        private void CreateMyListViewCoppy()
        {
            // Create a new ListView control.
            //ListView listView1 = new ListView();
            listView1.Bounds = new Rectangle(10, 10, 800, 400);

            // Set the view to show details.
            listView1.View = View.Details;
            //// Allow the user to edit item text.
            //listView1.LabelEdit = true;
            //// Allow the user to rearrange columns.
            //listView1.AllowColumnReorder = true;
            // Display check boxes.
            listView1.CheckBoxes = true;
            // Select the item and subitems when selection is made.
            listView1.FullRowSelect = true;
            //// Display grid lines.
            //listView1.GridLines = true;
            //// Sort the items in the list in ascending order.
            //listView1.Sorting = System.Windows.Forms.SortOrder.Ascending;


            // Create three items and three sets of subitems for each item.
            ListViewItem item1 = new ListViewItem("1XXXXXXXXXXXXXXXXXXXXXXXXXXXXXyyyyyyyyyyyyyyyyyXX");
            // Place a check mark next to the item.
            item1.Checked = true;
            item1.ForeColor = Color.Red;
            item1.Tag = "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";

            //item1.SubItems.Add("1");
            //item1.SubItems.Add("2");
            //item1.SubItems.Add("3");
            ListViewItem item2 = new ListViewItem("item2");

            item2.SubItems.Add("4");
            item2.SubItems.Add("5");
            item2.SubItems.Add("6");
            ListViewItem item3 = new ListViewItem("item3");
            // Place a check mark next to the item.
            item3.Checked = true;
            item3.SubItems.Add("7");
            //item3.SubItems.Add("8");
            //item3.SubItems.Add("9");

            // Create columns for the items and subitems.
            // Width of -2 indicates auto-size.
            listView1.Columns.Add("Check List Details", 800, HorizontalAlignment.Left);
            //listView1.Columns.Add("Column 2", 100, HorizontalAlignment.Left);
            //listView1.Columns.Add("Column 3", 100, HorizontalAlignment.Left);
            //listView1.Columns.Add("Column 4", 100, HorizontalAlignment.Center);

            //Add the items to the ListView.these are rows
            listView1.Items.Add(item1);
            //listView1.Items.Add( item2);
            //listView1.Items.Add( item3);
        }

        private void btnSaveCheckList_Click(object sender, EventArgs e)
        {
            int i=0,iStatus=0;
            string strUpdate = "";
            for (i = 0; i < listView1.Items.Count; i++)
            {
                if (listView1.Items[i].Checked)
                {
                    iStatus = 1;
                }
                else
                {
                    iStatus = 0;
                }
                //strUpdate = strUpdate +"/"+ listView1.Items[i].Tag.ToString() + "/" + iStatus;
                strUpdate = strUpdate + "" + iStatus;
                //MessageBox.Show(listView1.Items[i].Text, listView1.Items[i].Tag.ToString() + "(" + listView1.Items[i].Checked.ToString()+")");
            }
            //MessageBox.Show(strUpdate, "update");
            string strStatus = cbStatus.SelectedItem.ToString().Substring(0, 1);
            String mainURL = "http://kpcqms.com:7090/handheld/checklists/";
            String dURL = mainURL +VehicleTag+"/"+moduleQMS.gCheckpoint+"/"+ strUpdate+"/"+strStatus;
            string Ret = Cons.HttpPost(dURL, " ");

            int pos = Ret.IndexOf(":", 0);
            string sRet = Ret.Substring(pos + 1, 1);
            //if (sRet == "1")
            //{
            //    string strMess = "";
            //    pos = Ret.IndexOf(":", pos + 1);
            //    strMess = Ret.Substring(pos + 2);
            //    strMess = strMess.Substring(0, strMess.Length - 2);
            //    MessageBox.Show(strMess);
            //}
            //else
            //{
            //}

            MessageBox.Show(Ret, "return",
                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //listView1.Refresh();
                //listView1.Update(); 
                //listView1.ResumeLayout();
                int  isellectedItem = listView1.SelectedIndices[0];
                //MessageBox.Show(listView1.Items[isellectedItem].Text, listView1.Items[isellectedItem].Checked.ToString());
                lbChekList.Text = listView1.Items[isellectedItem].Text;
            }
            catch
            {
            }
                
        }

        private void listView1_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            //MessageBox.Show(e.CurrentValue.ToString(),e.Index.ToString());
            if (e.CurrentValue == CheckState.Checked)
            {
                listView1.Items[e.Index].ForeColor = Color.Maroon;
            }
            else if (e.CurrentValue == CheckState.Unchecked)
            {
                listView1.Items[e.Index].ForeColor = Color.Blue;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            listView1.Clear();
            lbChekList.Text = "";
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                listView1.Clear();
                lbChekList.Text = "";
                Login myForm = new Login();
                this.Hide();
                myForm.ShowDialog();
                this.Close();
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
            }
        }

    }
}