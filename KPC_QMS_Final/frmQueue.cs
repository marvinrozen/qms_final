﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlServerCe;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Net;
using System.IO;
//using System.ServiceModel.Web;
using System.Runtime.Serialization;
using System.Configuration;
using System.Windows.Forms.Layout;
using System.Threading;
using Intermec.DataCollection.RFID;
using Intermec.DataCollection;  //for bar code class
using Intermec.DeviceManagement.SmartSystem;
using System.Collections.Specialized;

namespace KPC_QMS_Final
{
    public partial class frmQueue : Form
    {
        public frmQueue()
        {
            InitializeComponent();
            CreateRFIDReaderObject();

        }

        private void frmQueue_Load(object sender, EventArgs e)
        {

            String mainURL = "http://kpcqms.com:7090/handheld/";
            String dURL = mainURL + "checkpoint/" + moduleQMS.gCheckpoint;
            string Ret = HttpGet(dURL, " ");
            bool ret = jsonConverterQueue(Ret);
            //MessageBox.Show(Ret, "return",
            //       MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);

            SqlCeDataAdapter adapter = new SqlCeDataAdapter();

            SqlCeConnection conn = new SqlCeConnection(Cons.connStr);
            conn.Open();
            string query = "select * from [queue] where (broad_queue = 1)";
            adapter.SelectCommand = new SqlCeCommand(query, conn);

            DataSet dset = new DataSet();
            adapter.Fill(dset);

            dgLocal1.DataSource = dset.Tables[0];


            adapter = new SqlCeDataAdapter();

            conn = new SqlCeConnection(Cons.connStr);
            conn.Open();
            query = "select * from [queue] where (broad_queue = 2)";
            adapter.SelectCommand = new SqlCeCommand(query, conn);

            DataSet dset2 = new DataSet();
            adapter.Fill(dset2);

            dgLocal2.DataSource = dset2.Tables[0];
            //string constr = Cons.connStr;
            //string query = "SELECT * FROM [queue];";
            //query += "SELECT * FROM [queue]";

            //using (SqlConnection con = new SqlConnection(constr))
            //{
            //    using (SqlCommand cmd = new SqlCommand(query))
            //    {
            //        using (SqlDataAdapter sda = new SqlDataAdapter())
            //        {
            //            cmd.Connection = con;
            //            sda.SelectCommand = cmd;
            //            using (DataSet ds = new DataSet())
            //            {
            //                sda.Fill(ds);
            //                dgLocal1.DataSource = ds.Tables[0];
            //                dgExport.DataSource = ds.Tables[1];
            //            }
            //        }
            //    }
            //}
        }
        
        string HttpGet(string uri, string parameters)
        {
            var request = WebRequest.Create(uri);

            var response = request.GetResponse();

            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

            return responseString;
        }


        public static void ExecuteDML(string sql)
        {
            SqlCeCommand myCommand = new SqlCeCommand(sql);
            SqlCeConnection conn = new SqlCeConnection(Cons.connStr);
            conn.Open();
            myCommand.Connection = conn;
            //connOpen();
            myCommand.ExecuteNonQuery();
            conn.Close();
        }
        public bool jsonConverterQueue(string strJson)
        {
            string jsonStr = strJson;//string to manipulate
            string squeueNo = "", somcCode = "";
            string svehicleNo = "", svehicleTag = "";
            string sdriverName = "", stransporter = "", sloading = "", sexport = "", scriteria = "", sbroad_queue = "";
            bool bExit = false;
            string sQuery = "";
            jsonStr = jsonStr.Replace("[", "");
            jsonStr = jsonStr.Replace("]", "");
            jsonStr = jsonStr.Replace("{", "");
            jsonStr = jsonStr.Replace("}", "");
            jsonStr = jsonStr.Replace("\"", "");
            jsonStr = jsonStr + ",";
            int i = 1;
            //delete statement
            string sSqlDel = "DELETE [queue]";
            ExecuteDML(sSqlDel);
            while (!bExit)
            {
                int pos = 0;
                int posNext = 0;
                int valueLen = 0;
                string strval = "";//posNext-pos+1);
                int cutPost = 0;

                for (i = 1; i <= 10; i++)
                {
                    pos = jsonStr.IndexOf(":", 0);
                    if (pos == -1)
                    {
                        bExit = true;
                        break;
                    }
                    posNext = jsonStr.IndexOf(",", 0);
                    valueLen = posNext - pos - 1;
                    strval = jsonStr.Substring(pos + 1, valueLen);
                    if (i % 10 == 1)
                    {
                        squeueNo = strval;
                    }
                    else if (i % 10 == 2)
                    {
                        somcCode = strval;
                    }
                    else if (i % 10 == 3)
                    {
                        svehicleNo = strval;
                    }
                    else if (i % 10 == 4)
                    {
                        svehicleTag = strval;
                    }
                    else if (i % 10 == 5)
                    {
                        sdriverName = strval;
                    }
                    else if (i % 10 == 6)
                    {
                        stransporter = strval;
                    }
                    else if (i % 10 == 7)
                    {
                        sloading = strval;
                    }
                    else if (i % 9 == 8)
                    {
                        sexport = strval;
                    }
                    else if (i % 9 == 9)
                    {
                        scriteria = strval;
                    }
                    else//mod =0
                    {
                        sbroad_queue = strval;
                    }

                    cutPost = posNext + 1;
                    jsonStr = jsonStr.Substring(cutPost);
                }
                if (!bExit)
                {
                    string sSql = "INSERT INTO [queue] (queueNo, omcCode, vehicleNo, vehicleTag, driverName, transporter, loading, export, criteria, broad_queue) VALUES (" + squeueNo + ",'" + somcCode + "','" + svehicleNo + "','" + svehicleTag + "','" + sdriverName + "','" + stransporter + "','" + sloading + "'," + sexport + ",'" + scriteria + "'," + sbroad_queue + ")";
                    ExecuteDML(sSql);
                    sQuery = sQuery + "(" + squeueNo + "," + somcCode + "," + svehicleNo + "," + svehicleTag + "," + sdriverName + "," + stransporter + "," + sloading + "," + sexport + "," + scriteria + "," + sbroad_queue + "),";
                }
                i = 1;
            }
            return true;
        }


        private void button2_Click(object sender, EventArgs e)
        {
            String mainURL = "http://172.18.62.156/qms_l/public/handheld/";
            String dURL = mainURL + "checkpoint/1";
            string Ret = HttpGet(dURL, " ");
            bool ret = jsonConverterQueue(Ret);
            MessageBox.Show(Ret, "return",
                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        //private void frmQueue_Load(object sender, EventArgs e)
        //{
        //    // TODO: This line of code loads data into the 'test7DataSet.queue' table. You can move, or remove it, as needed.
        //    this.queueTableAdapter.Fill(this.test7DataSet.queue);

        //}

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            SqlCeDataAdapter adapter = new SqlCeDataAdapter();

            SqlCeConnection conn = new SqlCeConnection(Cons.connStr);
            conn.Open();
            string query = "select * from [queue] where export=0";
            adapter.SelectCommand = new SqlCeCommand(query, conn);

            DataSet dset = new DataSet();
            adapter.Fill(dset);

            dgLocal1.DataSource = dset.Tables[0];
           
        }

        private void button3_Click(object sender, EventArgs e)
        {
            String mainURL = "http://172.18.62.156/qms_l/public/handheld/";
            String dURL = mainURL + "checkpoint/1";
            string Ret = HttpGet(dURL, " ");
            bool ret = jsonConverterQueue(Ret);
            MessageBox.Show(Ret, "return",
                   MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Login myForm = new Login();
            this.Hide();
            myForm.ShowDialog();
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Login myForm = new Login();
            this.Hide();
            myForm.ShowDialog();
            this.Close();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //frmChecklist myForm = new frmChecklist();
            //this.Hide();
            //myForm.ShowDialog();
            //this.Close();
        }

     
        //RFID Readings
        //define reader object
        public BRIReader brdr = null;

#if USE_BARCODESCANNER
        //bar code scanner object
        protected BarcodeReader myBCScanner = new BarcodeReader();
#endif

        //private bool bBarCodeScannerOn = false;
        private bool bScanBarCode = false;
        bool bContinuousReadEnabled = false;
        bool bReaderOffLine = true;
        private string[] MyTagList = new string[100];
        private int iBarCodeCount = 0;
        private int iRFIDTagCount = 0;
        

        private void GetCN70CofigurationString()
        {
            String xmlCN = "";  //XML Strings to search with.
            //String xmlOS = "";
            //String xmlSSPB = "";
            //String xmlIP = "";
            //String xmlBT = "";
            //String xmlI2of5 = "";
            //String xmlBacklight = "";
            string sConfig = null;
            String sVal = "";   //Holds the returned string from smart systems.
            int iIdx = 0;       //Index into string for the key we want.

            uint uiRet = 0;
            int iLen = 1024;
            StringBuilder sbRetData = new StringBuilder(1024);

            ITCSSApi SS = new ITCSSApi();  //Create the SmartSystems handle

            //Get the CN# from the device, parse out all the other XML

            xmlCN = "<Subsystem Name=\"SS_Client\">\r\n";
            xmlCN += "<Group Name=\"Identity\">\r\n";
            xmlCN += "<Field Name=\"HardwareVersion\">{0}</Field>\r\n";
            xmlCN += "</Group></Subsystem>";

            try
            {
                uiRet = SS.Get(xmlCN, sbRetData, ref iLen, 0);
                sVal = sbRetData.ToString();
                iIdx = sVal.IndexOf("<Field Name=\"HardwareVersion\">");

                if (iIdx >= 0)
                {
                    sVal = sVal.Substring(iIdx);  //Get all characters starting with Hardware version
                    sVal = sVal.Remove(0, 30);
                    iIdx = sVal.IndexOf("<");
                    sVal = sVal.Substring(0, iIdx);
                }
                else
                {
                    MessageBox.Show("CN# not found");
                }

                /*
                //Sample config string for FCC Bali CN70EQ4KC14W2100
                non-bali Cn70 example: CN70EQ4KCD6W2100
                For the configurations above, the “D6 “ (CDMA-Verizon WAN option) code would 
                change to “14” for RFID-FCC ,US,CAN/MEX), there will be an “02” for ETSI units.   
                */

                sConfig = sVal.Substring(9, 2);

                PostMessageToListBox1("Config String:");
                PostMessageToListBox1("->" + sVal);
                PostMessageToListBox1("->" + sConfig);

                if (sConfig.Equals("14"))
                {
                    //FCC unit
                }
                else if (sConfig.Equals("02"))
                {
                    //ETSI unit
                }
                else
                {
                    //no bali reader
                    MessageBox.Show("No RFID reader found!.\r\n\r\nApplication will close", "Error", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show(ee.ToString());
            }

            SS = null;
        }

        private void CreateRFIDReaderObject()
        {
            VerifyReaderPlatform();

            GetCN70CofigurationString();

            bool bStatus = OpenReaderConnection();

            if (bStatus == true)
            {
                if (!brdr.IsRFIDButtonEnabled(BRIReader.RFIDButtonIDs.MIDDLE))
                    brdr.RFIDButtonEnable(BRIReader.RFIDButtonIDs.MIDDLE);

                AddRFIDEventHandlers();

                SetReaderAttributes();
            }
        }

        private void VerifyReaderPlatform()
        {
            string smsg = null;

            try
            {
                //DEVICE 1 ATTRIB ADMINSTATUS: If this returns ON, it means the device is enabled.  OFF indicates disabled.
                //DEVICE 1 ATTRIB OPERSTATUS: If this returns ON, it means the device is connected.  OFF indicates disconnected.
                //DEVICE 1 ATTRIB DEVICETYPE: This will return the string representing the attached device, IM11, IP30, etc.

                Intermec.DataCollection.RFID.BasicBRIReader DCEConfig = new BasicBRIReader(null);
                DCEConfig.Open("TCP://127.0.0.1:2188");

                smsg = DCEConfig.Execute("DEVICE 1 ATTRIB ADMINSTATUS");
                if (smsg.IndexOf("OFF") >= 0)
                {
                    string s = DCEConfig.Execute("device 1 attrib adminstatus=on");	// BRI ‘device’ command.
                    //MessageBox.Show("RFID Device is disabled. Check Intermec Settings.", "WARNING");
                    //return false;
                }
                smsg = DCEConfig.Execute("DEVICE 1 ATTRIB OPERSTATUS");
                if (smsg.IndexOf("OFF") >= 0)
                {
                    MessageBox.Show("RFID Device is disconnected. Check Intermec Settings.", "WARNING");
                    //return false;
                }

                //get which device is selected in Intermec Settings
                smsg = DCEConfig.Execute("DEVICE 1 ATTRIB DEVICETYPE");
                if (smsg.IndexOf("IM11") >= 0)
                {
                    //TODO:
                }
                else if (smsg.IndexOf("IP30") >= 0)
                {
                    //TODO:
                }

                //Set the device type in Intermec Settings.
                //smsg = DCEConfig.Execute("DEVICE 1 ATTRIB DEVICETYPE=IM11");
                //smsg = DCEConfig.Execute("DEVICE 1 ATTRIB DEVICETYPE=IP30");
                //if (smsg.IndexOf("OK>") >= 0)
                //{
                //    //TODO:
                //}

                DCEConfig.Close();
                DCEConfig = null;
            }
            catch (Exception e)
            {
                MessageBox.Show("Unable to activate DCE reader connection." +
                "  Verify that the reader is connected and its battery is charged." +
                "  You may not be able to open a reader connection", "DCEConfig.Execute Exception", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }

        }

        private void SetReaderAttributes()
        {
            string sRsp = null;

            //configure IP30.  These settings will only persist for this current session.

            try
            {
                this.brdr.Execute("ATTRIB schedopt=0"); //0,1,2
                this.brdr.Execute("ATTRIB session=0");  //0,1,2,3

                this.brdr.Execute("ATTRIB IDTRIES=1");
                this.brdr.Execute("ATTRIB ANTTRIES=1");
                //this.brdr.Execute("ATTRIB IDTIMEOUT=1000");
                //this.brdr.Execute("ATTRIB ANTTIMEOUT=1000");
                this.brdr.Execute("ATTRIB TIMEOUTMODE=ON"); //ONLY NEED TO DO THIS WHEN SCHEDOPT=0 OTHERWISE THIS ATTRIB IS IGNORED BY THE READER.
                this.brdr.Execute("ATTRIB WRTRIES=3");
                this.brdr.Execute("ATTRIB TAGTYPE=EPCC1G2");

                //get the list of all attributes from the reader and display them.
                sRsp = this.brdr.Execute("ATTRIB");
                ParseResponseMessage(sRsp);
            }
            catch (Intermec.DataCollection.RFID.BasicReaderException ex)
            {
                MessageBox.Show("SetAttribute Exception : " + ex.Message);
            }
        }

#if USE_BARCODESCANNER
        private void CreateBarCodeScannerObject()
        {
            //add bar code scanner object and start thread so we can capture bar code events.
            myBCScanner.BarcodeRead += new BarcodeReadEventHandler(myBCScanner_BarcodeRead);
            myBCScanner.ThreadedRead(true);
            myBCScanner.ScannerEnable = true;
        }
#endif

#if USE_BARCODESCANNER
        void myBCScanner_BarcodeRead(object sender, BarcodeReadEventArgs bre)
        {
            display bar code when it gets read

            myBCScanner.ScannerOn = false;

            iBarCodeCount++;
            PostMessageToListBox1(iBarCodeCount + ". " + bre.strDataBuffer);
            label1.Text = "Bar Code Count = " + iBarCodeCount;
        }
#endif

        private void ParseResponseMessage(string sMsg)
        {
            int x = 0;
            string delimStr = null;
            string[] tList = null;
            char[] delimiter = null;
            int RspCount = 0;

            delimStr = "\n";
            delimiter = delimStr.ToCharArray();
            tList = sMsg.Replace("\r\n", "\n").Split(delimiter);
            RspCount = tList.Length;
            for (x = 0; x < RspCount; x++)
            {
                PostMessageToListBox1(tList[x]);
            }
        }

        private bool OpenReaderConnection()
        {
            //Establish connection with reader.

            string sMsg = null;
            bool bStatus = true;

            //define connection
            string sConnection = null;    //handheld readers only

            //enable IDL debug logging ***********************************************
            BRIReader.LoggerOptionsAdv LogOp = new BRIReader.LoggerOptionsAdv();
            LogOp.LogFilePath = ".\\IDLClassDebugLog.txt";
            LogOp.ShowNonPrintableChars = true;
            //************************************************************************

            bReaderOffLine = true;

            try
            {
                //open reader connection
                //option one -> simple
                //brdr = new BRIReader(this, sConnection);

                //option two -> set size of reader buffer, event buffer, and enable IDL logging.
                //Reader Buffer is used for storing tags when you issue a READ, or READ REPORT=NO
                //Event Buffer is used for storing tags when you issue a READ REPORT=EVENT and all other events.
                //this.brdr = new BRIReader(this, sConnection, Reader Buffer, Event Buffer, LogOp);
                this.brdr = new BRIReader(this, sConnection, 1000, 1000, LogOp);
            }
            catch (BasicReaderException ex)
            {
                MessageBox.Show(ex.ToString());
                bStatus = false;
                bReaderOffLine = true;
                return bStatus;
            }

            if (brdr == null || bStatus == false)
            {
                //failed to create reader connection
                PostMessageToListBox1("Unable to connect to hand held");
                bReaderOffLine = true;
                bStatus = false;
            }
            else if (brdr.IsConnected)
            {
                //Connected to DCE but not necessarily to the reader.  We need to verify
                //that we are actually talking to the RF module.  Should return OK>
                if (this.brdr.Execute("PING").Equals("OK>"))
                {
                    //get reader firmware version
                    sMsg = this.brdr.Execute("VER");
                    ParseResponseMessage(sMsg);
                    bReaderOffLine = false;
                    bStatus = true;
                }
                else
                {
                    //not connected to reader
                    PostMessageToListBox1("Unable to connect to hand held");
                    bReaderOffLine = true;
                    bStatus = false;
                }
            }
            return bStatus;
        }

        private void PostMessageToListBox1(string sMsg)
        {
            listBox1.Items.Add(sMsg);////////////////////////////////////////////////////////
            listBox1.SelectedIndex = listBox1.Items.Count - 1;
            listBox1.Refresh();
        }


        private void CloseReaderConnection()
        {
            if (brdr != null) { brdr.Dispose(); }
        }

        private void AddRFIDEventHandlers()
        {
            //Add IDL event handlers
            brdr.EventHandlerCenterTrigger += new CenterTrigger_EventHandlerAdv(brdr_EventHandlerCenterTrigger);
            brdr.EventHandlerRFIDButton += new DCE_BUTTON_EventHandlerAdv(brdr_EventHandlerRFIDButton);
            brdr.EventHandlerDeviceConnectState += new DCE_DeviceConnectStateEventHandlerAdv(brdr_EventHandlerDeviceConnectState);
            brdr.EventHandlerTag += new Tag_EventHandlerAdv(brdr_EventHandlerTag);
        }

        void brdr_EventHandlerRFIDButton(object sender, EVTADV_RFIDButton_EventArgs EvtArgs)
        {
            if (bReaderOffLine) return;

            if (EvtArgs.ButtonState.Equals(EVTADV_CenterTrigger_EventArgs.STATE.PULLED) ||
                EvtArgs.ButtonState.Equals(EVTADV_RFIDButton_EventArgs.RFIDButtonStates.PRESSED))
            {
                if (!bScanBarCode)
                {
                    ReadTagsReportDirect();
                    //ReadTagsReportEvent();
                    //ReadTagsReportNo();
                    //WriteTag();
                }

#if USE_BARCODESCANNER
                if (bScanBarCode)
                {
                    myBCScanner.ScannerOn = true;
                    bBarCodeScannerOn = true;
                }
#endif

            }
            else if (EvtArgs.ButtonState.Equals(EVTADV_CenterTrigger_EventArgs.STATE.RELEASED) ||
                EvtArgs.ButtonState.Equals(EVTADV_RFIDButton_EventArgs.RFIDButtonStates.RELEASED))
            {
#if USE_BARCODESCANNER
                if (bBarCodeScannerOn)
                    myBCScanner.ScannerOn = false;
                bBarCodeScannerOn = false;
#endif

                if (bContinuousReadEnabled)
                    brdr.StopReadingTags();

            }
        }

        void brdr_EventHandlerTag(object sender, EVTADV_Tag_EventArgs EvtArgs)
        {
            //Used with ReadTagsReportEvent() with BRIReader.TagReportOptions.EVENT

            string sTagData = null;
            string[] myFields = new string[10];  //setting length arbitrarily

            iRFIDTagCount++;

            sTagData = EvtArgs.DataString.ToString();
            PostMessageToListBox1(iRFIDTagCount + ". " + sTagData);

            //IF you are reading fields such as COUNT, ANT, TIME, RSSI, that data comes back as fields.
            //sTagData = EvtArgs.Tag.ToString();
            //for (int x = 0; x < EvtArgs.Tag.TagFields.ItemCount; x++)
            //{
            //    myFields[x] = EvtArgs.Tag.TagFields.FieldArray[x].DataString;
            //}
            //PostMessageToListBox1(iRFIDTagCount + ". " + sTagData);
            //label2.Text = "RFID Tag Count = " + iRFIDTagCount;
            //I'm not displaying field data, I'll leave that to you.
        }

        void brdr_EventHandlerDeviceConnectState(object sender, EVTADV_DeviceConnectStateEventArgs EvtArgs)
        {
            //You need to monitor the connection between the hand held reader and the DCE on the terminal.

            switch (EvtArgs.DeviceConnectState)
            {
                case EVTADV_DeviceConnectStateEventArgs.States.OFFLINE:
                    bReaderOffLine = true;
                    break;
                case EVTADV_DeviceConnectStateEventArgs.States.RECONNECTING:
                    bReaderOffLine = true;
                    break;
                case EVTADV_DeviceConnectStateEventArgs.States.CONNECTED:
                    bReaderOffLine = false;
                    break;
            }
        }

        void brdr_EventHandlerCenterTrigger(object sender, EVTADV_CenterTrigger_EventArgs EvtArgs)
        {
            //*********************************************************************
            // This function fires when the center trigger on the handheld is pulled or released
            //*********************************************************************

            return;

            //make sure reader is online before issueing any RF commands.
            if (bReaderOffLine == true)
                return;

            if (EvtArgs.CenterTriggerState.Equals(EVTADV_CenterTrigger_EventArgs.STATE.PULLED))
            {
                if (!bScanBarCode)
                {
                    ReadTagsReportDirect();
                    ReadTagsReportEvent();
                    ReadTagsReportNo();
                    WriteTag();
                }

#if USE_BARCODESCANNER
                if (bScanBarCode)
                {
                    myBCScanner.ScannerOn = true;
                    bBarCodeScannerOn = true;
                }
#endif
            }
            else if (EvtArgs.CenterTriggerState.Equals(EVTADV_CenterTrigger_EventArgs.STATE.RELEASED))
            {
#if USE_BARCODESCANNER
                if (bBarCodeScannerOn)
                    myBCScanner.ScannerOn = false;
                bBarCodeScannerOn = false;
#endif

                if (bContinuousReadEnabled)
                    brdr.StopReadingTags();
            }
        }

        private int ReadTagsReportDirect()
        {
            //Simple read

            int iTagCount = 0;
            bool bStatus = false;

            bContinuousReadEnabled = false;

            //Here are various read options
            //Pick one and comment out the other two

            //1. Simple read of epc id's
            bStatus = brdr.Read();

            //2. Read only tags who's epc id starts with hex 0102
            //bStatus = brdr.Read("HEX(1:4,2)=H0102");

            //3. Return the antenna that read the tag and the number of times each tag was read
            //bStatus = brdr.Read(null, "ANT COUNT");

            //get the tag ids
            iTagCount = LoadTagList();

            return iTagCount;
        }

        private int ReadTagsReportNo()
        {
            //Continuous read using polling to retrieve tag list

            bool bStatus = false;

            bContinuousReadEnabled = true;

            //determine how much time you wait before polling for tag list.
            //here it is set to 1 second
            timer1.Interval = 1000;

            //Here are various continuous read options
            //Pick one and comment out the other two
            //Don't forget you must issues a brdr.StopReadingTags() to turn off the RF when you are done reading.

            //1. Read epc id's
            bStatus = brdr.StartReadingTags(null, null, BRIReader.TagReportOptions.POLL);
            //2. Use a filter to read only tags who's epc id starts with hex 0102
            bStatus = brdr.StartReadingTags("HEX(1:4,2)=H0102", null, BRIReader.TagReportOptions.POLL);
            //3. Return the antenna that read the tag and the number of times each tag was read
            bStatus = brdr.StartReadingTags(null, "ANT COUNT", BRIReader.TagReportOptions.POLL);

            //enable timer to poll for tags
            timer1.Enabled = true;

            return 0;
        }

        private void ReadTagsReportEvent()
        {
            //Continuous Read which returns tags as events

            bool bStatus = false;

            bContinuousReadEnabled = true;

            //Here are various read options
            //Pick one and comment out the other two
            //Don't forget you must issues a brdr.StopReadingTags() to turn off the RF when you are done reading.

            //1. Read epc id's
            bStatus = brdr.StartReadingTags(null, null, BRIReader.TagReportOptions.EVENT);
            //2. Use a filter to read only tags who's epc id starts with hex 0102
            //bStatus = brdr.StartReadingTags("HEX(1:4,2)=H0102", null, BRIReader.TagReportOptions.EVENT);
            //3. Return the antenna that read the tag and the number of times each tag was read
            //bStatus = brdr.StartReadingTags(null, "ANT COUNT", BRIReader.TagReportOptions.EVENT);
        }

        private int LoadTagList()
        {
            //load epc ids into a list

            int iTagCount = 0;

            if (brdr.TagCount > 0)
            {
                foreach (Tag tt in brdr.Tags)
                {
                    iRFIDTagCount++;
                    MyTagList[++iTagCount] = tt.ToString();

                    if (tt.TagFields.ItemCount > 0)
                    {
                        foreach (TagField tf in tt.TagFields.FieldArray)
                        {
                            if (tf.Status < 0)
                            {
                                //code to handle read or write error on this field
                            }
                            else
                            {
                                //get field data such as ANT, COUNT, TIME, AFI, etc.
                                MyTagList[iTagCount] += " " + tf.ToString();
                            }
                        }
                    }
                    PostMessageToListBox1(MyTagList[iTagCount]);

                    // Marvin3
                    string strLastTag=this.listBox1.Items[this.listBox1.Items.Count-1].ToString();
                    string checkcommand = "SELECT vehicleTag,vehicleNo FROM [queue] WHERE (vehicleTag ='" + strLastTag + "')";
                    DataTable dt =new DataTable() ;
                    moduleQMS.reurnSqlCeSrvRows(dt, checkcommand);
                    
                    //SqlCeDataReader reader = moduleQMS.SelectSqlCeSrvRowsQMS(checkcommand);
                    if (dt.Rows.Count >0)
                    {
                        //
                        FormChecklist.VehicleReg = dt.Rows[0]["vehicleNo"].ToString();
                        FormChecklist.VehicleTag = dt.Rows[0]["vehicleTag"].ToString();
                        dt.Dispose();
                        moduleQMS.connCloseQMS();
                        //---
                        FormChecklist.GetInstance.Show(); this.Hide();
                    }
                    else
                    {
                        dt.Dispose();
                        moduleQMS.connCloseQMS();
                    }
                 //   SqlCeConnection cn = new SqlCeConnection(Cons.connStr);
                 //   string checkcommand = "SELECT vehicleTag FROM [queue] WHERE (vehicleTag = @vehicleTag)";
                 //   SqlCeCommand checkcmd = new SqlCeCommand(checkcommand, cn);
                 //   checkcmd.Parameters.AddWithValue("@vehicleTag", this.listBox1.Items.Count - 1);
                 //   cn.Open();
                 //   //Check if tag already exist
                 //   if (checkcmd.ExecuteNonQuery() > 0)
                 //   {
                 //       MessageBox.Show("Tag Id Exists.", "return",
                 //MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                 //   }
                 //   else
                 //   {
                 //       MessageBox.Show("Tag Id doesnt exist.", "return",
                 // MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                 //   }

                }
            }
            return iTagCount;
        }

        private void WriteTag()
        {
            string sRSP = null;

            //Pick a method to use for writing tags.  Below or FOUR examples of different writing methods.
            //1. Issue simple write command to program the EPC code in a tag.  This write command will write to
            //all tags found.
            sRSP = this.brdr.Execute("W EPCID=H010203040506070809101112");

            //2. Program an EPC code using a where clause to select a subgroup of tags.
            //sRSP = this.brdr.Execute("W EPCID=H010203040506070809101112 WHERE HEX(1:4,2)==H1122");

            //3. Issue simple write command which writes the ascii string "KAG123R" to the tag.  This write command
            //will write to all tags found.
            //sRSP = this.brdr.Execute("W STRING(1:4,2)=\"KAG123R\"");


            //4. Issue write command which writes the ascii string "HI" to the all tags using a where clauise to select
            //all tags that do not have "HI" already programmed into them.  This write command
            //will write to all matching the where clause.
            //sRSP = this.brdr.Execute("W STRING(1:4,2)=\"HI\" WHERE STRING(1:4,2)!=\"HI\"");



            ParseResponseMessage(sRSP);

            //you need to check the response to determine if the write was successful
            if (sRSP.IndexOf("WROK") > 0)
            {
                //write succeeded.  Add your code here.
                iRFIDTagCount++;
            }
            else if (sRSP.IndexOf("WRERR") > 0)
            {
                //Write failed.  Add your code here.

            }
            else if (sRSP.IndexOf("ERR") > 0)
            {
                //catch any other type of error that could occur during the write operation.   Add your code here.

            }
            else if (sRSP.IndexOf("ERR") == 0)
            {
                //syntax error in command.  Add your code here.

            }
        }

        private void PollForTags()
        {
            bool bStatus = false;
            int iTagCount = 0;

            //get tag list from reader
            bStatus = brdr.PollTags();

            //get the tag ids
            iTagCount = LoadTagList();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //enabled by ReadTagsReportNo() function
            //timer used to poll for tags
            //default interval is 1 second

            timer1.Enabled = false;

            PollForTags();

            timer1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //close down rfid and bar code then exit app
            if (this.brdr != null) { this.brdr.Dispose(); }
            this.brdr = null;

#if USE_BARCODESCANNER
            //hangs up debugger and screws up cn70
            //if (myBCScanner != null) { myBCScanner.Dispose(); }
            //myBCScanner = null;
#endif

            Application.Exit();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            //clear button
            //iRFIDTagCount = 0;
            //iBarCodeCount = 0;
            //listBox1.Items.Clear();
            //label1.Text = "Bar Code Count = " + iBarCodeCount;
            //label2.Text = "RFID Tag Count = " + iRFIDTagCount;//////////////////////////////////////
        }


        private void button4_Click(object sender, EventArgs e)
        {
            Login myForm = new Login();
            this.Hide();
            myForm.ShowDialog();
            this.Close();
        }


   


    }
}

