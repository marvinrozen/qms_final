﻿namespace KPC_QMS_Final
{
    partial class TagCreate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.writeLog = new System.Windows.Forms.ListBox();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.SearchReg = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnWrite = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.timer1 = new System.Windows.Forms.Timer();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // writeLog
            // 
            this.writeLog.Location = new System.Drawing.Point(4, 177);
            this.writeLog.Name = "writeLog";
            this.writeLog.Size = new System.Drawing.Size(231, 30);
            this.writeLog.TabIndex = 29;
            // 
            // listBox2
            // 
            this.listBox2.Location = new System.Drawing.Point(4, 31);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(163, 30);
            this.listBox2.TabIndex = 28;
            // 
            // SearchReg
            // 
            this.SearchReg.BackColor = System.Drawing.Color.Red;
            this.SearchReg.Location = new System.Drawing.Point(173, 31);
            this.SearchReg.Name = "SearchReg";
            this.SearchReg.Size = new System.Drawing.Size(64, 20);
            this.SearchReg.TabIndex = 27;
            this.SearchReg.Text = "Search";
            this.SearchReg.Click += new System.EventHandler(this.SearchReg_Click);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(4, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 20);
            this.label3.Text = "Serial No.";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(92, 4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(145, 21);
            this.textBox1.TabIndex = 26;
            // 
            // btnWrite
            // 
            this.btnWrite.Location = new System.Drawing.Point(4, 220);
            this.btnWrite.Name = "btnWrite";
            this.btnWrite.Size = new System.Drawing.Size(61, 20);
            this.btnWrite.TabIndex = 25;
            this.btnWrite.Text = "write";
            this.btnWrite.Click += new System.EventHandler(this.btnWrite_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Red;
            this.button4.Location = new System.Drawing.Point(3, 245);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(62, 20);
            this.button4.TabIndex = 24;
            this.button4.Text = "Back";
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(3, 78);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(204, 18);
            this.label2.Text = "label2";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(3, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 18);
            this.label1.Text = "label1";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(187, 220);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(48, 20);
            this.button2.TabIndex = 22;
            this.button2.Text = "CLEAR";
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.listBox1.Location = new System.Drawing.Point(3, 98);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(234, 72);
            this.listBox1.TabIndex = 21;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Red;
            this.button3.Location = new System.Drawing.Point(127, 245);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(109, 20);
            this.button3.TabIndex = 23;
            this.button3.Text = "Start Inspection";
            this.button3.Visible = false;
            // 
            // TagCreate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.writeLog);
            this.Controls.Add(this.listBox2);
            this.Controls.Add(this.SearchReg);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnWrite);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.listBox1);
            this.Menu = this.mainMenu1;
            this.Name = "TagCreate";
            this.Text = "TagCreate";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox writeLog;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.Button SearchReg;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnWrite;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button3;
    }
}