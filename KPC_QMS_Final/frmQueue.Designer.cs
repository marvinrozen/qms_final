﻿namespace KPC_QMS_Final
{
    partial class frmQueue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.tabQueue = new System.Windows.Forms.TabControl();
            this.tabLocal = new System.Windows.Forms.TabPage();
            this.dgLocal2 = new System.Windows.Forms.DataGrid();
            this.button8 = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.button5 = new System.Windows.Forms.Button();
            this.dgLocal1 = new System.Windows.Forms.DataGrid();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabExport = new System.Windows.Forms.TabPage();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dgExport = new System.Windows.Forms.DataGrid();
            this.queueBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.test7DataSet = new KPC_QMS_Final.test7DataSet();
            this.queueTableAdapter = new KPC_QMS_Final.test7DataSetTableAdapters.queueTableAdapter();
            this.timer1 = new System.Windows.Forms.Timer();
            this.dgExport2 = new System.Windows.Forms.DataGrid();
            this.tabQueue.SuspendLayout();
            this.tabLocal.SuspendLayout();
            this.tabExport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.queueBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.test7DataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // tabQueue
            // 
            this.tabQueue.Controls.Add(this.tabLocal);
            this.tabQueue.Controls.Add(this.tabExport);
            this.tabQueue.Location = new System.Drawing.Point(0, 0);
            this.tabQueue.Name = "tabQueue";
            this.tabQueue.SelectedIndex = 0;
            this.tabQueue.Size = new System.Drawing.Size(240, 265);
            this.tabQueue.TabIndex = 0;
            // 
            // tabLocal
            // 
            this.tabLocal.Controls.Add(this.dgLocal2);
            this.tabLocal.Controls.Add(this.button8);
            this.tabLocal.Controls.Add(this.listBox1);
            this.tabLocal.Controls.Add(this.button5);
            this.tabLocal.Controls.Add(this.dgLocal1);
            this.tabLocal.Controls.Add(this.btnUpdate);
            this.tabLocal.Controls.Add(this.button2);
            this.tabLocal.Controls.Add(this.button1);
            this.tabLocal.Location = new System.Drawing.Point(0, 0);
            this.tabLocal.Name = "tabLocal";
            this.tabLocal.Size = new System.Drawing.Size(240, 242);
            this.tabLocal.Text = "Local";
            // 
            // dgLocal2
            // 
            this.dgLocal2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgLocal2.Location = new System.Drawing.Point(7, 91);
            this.dgLocal2.Name = "dgLocal2";
            this.dgLocal2.Size = new System.Drawing.Size(226, 60);
            this.dgLocal2.TabIndex = 27;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(187, 263);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(48, 20);
            this.button8.TabIndex = 26;
            this.button8.Text = "CLEAR";
            // 
            // listBox1
            // 
            this.listBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.listBox1.Location = new System.Drawing.Point(3, 169);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(230, 44);
            this.listBox1.TabIndex = 25;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(7, 218);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(38, 20);
            this.button5.TabIndex = 6;
            this.button5.Text = "Back";
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // dgLocal1
            // 
            this.dgLocal1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgLocal1.Location = new System.Drawing.Point(7, 8);
            this.dgLocal1.Name = "dgLocal1";
            this.dgLocal1.Size = new System.Drawing.Size(226, 60);
            this.dgLocal1.TabIndex = 5;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(129, 218);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(58, 20);
            this.btnUpdate.TabIndex = 4;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.Visible = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(51, 218);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(72, 20);
            this.button2.TabIndex = 3;
            this.button2.Text = "PullQueue";
            this.button2.Visible = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(193, 218);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(40, 20);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.Visible = false;
            // 
            // tabExport
            // 
            this.tabExport.Controls.Add(this.dgExport2);
            this.tabExport.Controls.Add(this.button7);
            this.tabExport.Controls.Add(this.button6);
            this.tabExport.Controls.Add(this.button4);
            this.tabExport.Controls.Add(this.button3);
            this.tabExport.Controls.Add(this.dgExport);
            this.tabExport.Location = new System.Drawing.Point(0, 0);
            this.tabExport.Name = "tabExport";
            this.tabExport.Size = new System.Drawing.Size(240, 242);
            this.tabExport.Text = "Export";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(190, 219);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(43, 20);
            this.button7.TabIndex = 8;
            this.button7.Text = "Check";
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(4, 219);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(38, 20);
            this.button6.TabIndex = 7;
            this.button6.Text = "Back";
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(126, 220);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(57, 19);
            this.button4.TabIndex = 2;
            this.button4.Text = "Update";
            this.button4.Visible = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(48, 220);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(72, 19);
            this.button3.TabIndex = 1;
            this.button3.Text = "PullQueue";
            this.button3.Visible = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dgExport
            // 
            this.dgExport.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgExport.Location = new System.Drawing.Point(4, 4);
            this.dgExport.Name = "dgExport";
            this.dgExport.Size = new System.Drawing.Size(229, 74);
            this.dgExport.TabIndex = 0;
            // 
            // queueBindingSource
            // 
            this.queueBindingSource.DataMember = "queue";
            this.queueBindingSource.DataSource = this.test7DataSet;
            // 
            // test7DataSet
            // 
            this.test7DataSet.DataSetName = "test7DataSet";
            this.test7DataSet.Prefix = "";
            this.test7DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // queueTableAdapter
            // 
            this.queueTableAdapter.ClearBeforeFill = true;
            // 
            // dgExport2
            // 
            this.dgExport2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dgExport2.Location = new System.Drawing.Point(4, 106);
            this.dgExport2.Name = "dgExport2";
            this.dgExport2.Size = new System.Drawing.Size(229, 74);
            this.dgExport2.TabIndex = 9;
            // 
            // frmQueue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.tabQueue);
            this.KeyPreview = true;
            this.Menu = this.mainMenu1;
            this.Name = "frmQueue";
            this.Text = "frmQueue";
            this.Load += new System.EventHandler(this.frmQueue_Load);
            this.tabQueue.ResumeLayout(false);
            this.tabLocal.ResumeLayout(false);
            this.tabExport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.queueBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.test7DataSet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabQueue;
        private System.Windows.Forms.TabPage tabLocal;
        private System.Windows.Forms.TabPage tabExport;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private test7DataSet test7DataSet;
        private System.Windows.Forms.BindingSource queueBindingSource;
        private KPC_QMS_Final.test7DataSetTableAdapters.queueTableAdapter queueTableAdapter;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.DataGrid dgLocal1;
        private System.Windows.Forms.DataGrid dgExport;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.DataGrid dgLocal2;
        private System.Windows.Forms.DataGrid dgExport2;
    }
}