﻿using System;

using System.Collections.Generic;
using System.Text;
using System.Data.SqlServerCe;
using System.Data;
using System.Windows.Forms;

namespace KPC_QMS_Final
{
    class moduleQMS
    {
        public static SqlCeDataReader SelectSqlCeSrvRowsQMS(string selectQuery)
        {
            SqlCeCommand myCommand =null;
            SqlCeDataReader myDataReader = null;
            try
            {
                myCommand = new SqlCeCommand(selectQuery);
                connOpenQMS();
                myCommand.Connection = conn;
                myDataReader= myCommand.ExecuteReader();
            }
            catch (Exception er)
            {
             //report thr error message and null return
                MessageBox.Show(er.Message + (er.InnerException != null ? er.InnerException.Message : string.Empty), "Error Saving", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
            }
            finally
            {
               // connCloseQMS();
                myCommand.Dispose();
                
            }
            return myDataReader;

        }
        public static void ExecuteDMLQMS(string sql)
        {
            SqlCeCommand myCommand = new SqlCeCommand(sql);
            myCommand.Connection = conn;
            connOpenQMS();
            myCommand.ExecuteNonQuery();
        }

        public static void ExecuteDMLQMS(SqlCeCommand myCommand)
        {
            connOpenQMS();
            myCommand.Connection = conn;
            myCommand.ExecuteNonQuery();
        }
        public static int gCheckpoint = 0;
        public static string connStrLocal = "Data Source = \\KPC_QMS.sdf";
        public static SqlCeConnection conn = null;
        public static void connOpenQMS()
        {
            if (conn == null || conn.State != System.Data.ConnectionState.Open)
            {
                conn = new SqlCeConnection(connStrLocal);
                conn.Open();
            }
        }
        public static void connCloseQMS()
        {
            if (conn != null && conn.State != System.Data.ConnectionState.Closed)
            {
                conn.Close();
            }
        }
        public static void reurnSqlCeSrvRows(DataTable dataTable, string query) //error here
        {
            SqlCeDataAdapter adapter = new SqlCeDataAdapter();
            connOpenQMS();
            adapter.SelectCommand = new SqlCeCommand(query, conn);
            adapter.Fill(dataTable);
        }

        public static void returnSqlCeSrvRows(DataSet dataSet, string query)
        {
            SqlCeDataAdapter adapter = new SqlCeDataAdapter();
            connOpenQMS();
            adapter.SelectCommand = new SqlCeCommand(query, conn);
            adapter.Fill(dataSet);
        }
    }
}
